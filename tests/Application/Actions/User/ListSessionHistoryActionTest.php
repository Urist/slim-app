<?php

declare(strict_types=1);

namespace Tests\Application\Actions\User;

use App\Application\Actions\ActionPayload;
use App\Domain\User\UserRepository;
use Tests\TestCase;

class ListSessionHistoryActionTest extends TestCase
{
    public function testAction(): void
    {
        $app = $this->getAppInstance();

        $rawHistory = [
            [
                'score' => '10',
                'date' => '1637591448'
            ],
            [
                'score' => '10',
                'date' => '1637591498'
            ]
        ];

        $expectedHistory = [
            'history' => [
                [
                    'score' => '10',
                    'date' => '1637591448'
                ],
                [
                    'score' => '10',
                    'date' => '1637591498'
                ]
            ]
        ];

        $container = $app->getContainer();

        $userRepositoryProphecy = $this->prophesize(UserRepository::class);
        $userRepositoryProphecy
            ->getSessionHistory(1)
            ->willReturn($rawHistory)
            ->shouldBeCalledOnce();

        $container->set(UserRepository::class, $userRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/user/1/history');
        $response = $app->handle($request);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, $expectedHistory);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertEquals($serializedPayload, $payload);
    }
}
