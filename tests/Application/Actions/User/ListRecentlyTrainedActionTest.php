<?php

declare(strict_types=1);

namespace Tests\Application\Actions\User;

use App\Application\Actions\ActionPayload;
use App\Domain\User\UserRepository;
use Tests\TestCase;

class ListRecentlyTrainedActionTest extends TestCase
{
    public function testAction(): void
    {
        $app = $this->getAppInstance();

        $recentlyTrained = ['cognition'];

        $container = $app->getContainer();

        $userRepositoryProphecy = $this->prophesize(UserRepository::class);
        $userRepositoryProphecy
            ->getRecentlyTrained(1)
            ->willReturn($recentlyTrained)
            ->shouldBeCalledOnce();

        $container->set(UserRepository::class, $userRepositoryProphecy->reveal());

        $request = $this->createRequest('GET', '/user/1/recentlyTrained');
        $response = $app->handle($request);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, $recentlyTrained);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertSame(200, $response->getStatusCode());
        $this->assertEquals($serializedPayload, $payload);
    }
}