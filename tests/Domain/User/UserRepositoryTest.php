<?php

declare(strict_types=1);

namespace Tests\Domain\User;

use App\Domain\User\UserRepository;
use App\Infrastructure\Database\MysqlConnector;
use Tests\TestCase;

class UserRepositoryTest extends TestCase
{
    public function testGetSessionHistory(): void
    {
        $history = [
            [
                'score' => '10',
                'date' => '1637591448'
            ],
            [
                'score' => '10',
                'date' => '1637591498'
            ]
        ];

        $userRepository = new UserRepository($this->mockConnector($history));
        $this->assertEquals($history, $userRepository->getSessionHistory(1));
    }

    public function testGetRecentlyTrained(): void
    {
        $recentlyTrained = ['cognition'];

        $userRepository = new UserRepository($this->mockConnector($recentlyTrained));
        $this->assertEquals($recentlyTrained, $userRepository->getRecentlyTrained(1));
    }

    private function mockConnector(array $returnData): MysqlConnector
    {
        $stmt = $this->createMock(\PDOStatement::class);

        $stmt->expects($this->any())
            ->method('fetchAll')
            ->willReturn($returnData);

        $connMock = $this->createMock(MysqlConnector::class);

        $connMock->expects($this->any())
            ->method('prepare')
            ->willReturn($stmt);

        return $connMock;
    }
}
