<?php

declare(strict_types=1);

namespace App\Domain\User;

use App\Infrastructure\Database\DbConnectorInterface;
use PDO;

class UserRepository
{
    private DbConnectorInterface $connector;

    public function __construct(DbConnectorInterface $connector)
    {
        $this->connector = $connector;
    }

    public function getRecentlyTrained(int $userId): ?array
    {
        $stmt = $this->connector->prepare(
            'SELECT dc.name
            FROM sessions s
            LEFT JOIN courses c on s.course_id = c.course_id
            LEFT JOIN exercises e on c.course_id = e.course_id
            LEFT JOIN domain_categories dc on e.category_id = dc.category_id
            WHERE s.user_id = :userId
            AND s.timestamp = (SELECT MAX(timestamp) FROM sessions WHERE user_id = :userId)
            ORDER BY s.timestamp DESC;'
        );
        $stmt->bindParam('userId', $userId, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_COLUMN) ?? [];
    }

    public function getSessionHistory(int $userId, int $limit = 12): ?array
    {
        $stmt = $this->connector->prepare(
            'SELECT score, timestamp as date FROM sessions WHERE user_id = :userId LIMIT :limit;'
        );
        $stmt->bindParam('userId', $userId, PDO::PARAM_INT);
        $stmt->bindParam('limit', $limit, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC) ?? [];
    }
}