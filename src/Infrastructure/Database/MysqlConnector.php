<?php

declare(strict_types=1);

namespace App\Infrastructure\Database;

use PDO;

class MysqlConnector implements DbConnectorInterface
{
    private PDO $pdo;

    public function __construct(string $database, string $host, int $port, string $username, string $password)
    {
        $this->pdo = new PDO(
            sprintf('mysql:dbname=%s;host=%s;port=%d', $database, $host, $port),
            $username,
            $password
        );
    }

    public function prepare(string $sql): \PDOStatement
    {
        return $this->pdo->prepare($sql);
    }
}
