<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface;

class ListSessionHistoryAction extends UserAction
{
    protected function action(): ResponseInterface
    {
        $userId = $this->resolveArg('id');

        return $this->respondWithData(
            [
                'history' => $this->userRepository->getSessionHistory((int)$userId)
            ]
        );
    }
}
