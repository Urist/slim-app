<?php

declare(strict_types=1);

namespace App\Application\Actions\User;

use Psr\Http\Message\ResponseInterface as Response;

class ListRecentlyTrainedAction extends UserAction
{
    protected function action(): Response
    {
        $userId = $this->resolveArg('id');

        return $this->respondWithData($this->userRepository->getRecentlyTrained((int)$userId));
    }
}
