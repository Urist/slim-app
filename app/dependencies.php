<?php

declare(strict_types=1);

use App\Application\Settings\SettingsInterface;
use App\Infrastructure\Database\DbConnectorInterface;
use App\Infrastructure\Database\MysqlConnector;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
    ]);
    $containerBuilder->addDefinitions([
        DbConnectorInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsInterface::class);

            $databaseSettings = $settings->get('database');

            return new MysqlConnector(
                $databaseSettings['database'],
                $databaseSettings['host'],
                $databaseSettings['port'],
                $databaseSettings['user'],
                $databaseSettings['pass']
            );
        }
    ]);
};
