<?php
declare(strict_types=1);

use App\Application\Settings\Settings;
use App\Application\Settings\SettingsInterface;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {

    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsInterface::class => function () {
            return new Settings([
                'displayErrorDetails' => true, // Should be set to false in production
                'logError'            => false,
                'logErrorDetails'     => false,
                'logger' => [
                    'name' => 'slim-app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'database' => [
                    'database' => $_ENV['MYSQL_DATABASE'] ?? 'nn',
                    'user' => $_ENV['MYSQL_REG_USER'] ?? 'root',
                    'pass' => $_ENV['MYSQL_ROOT_PASSWORD'] ?? 'secret',
                    'host' => $_ENV['MYSQL_HOST'] ?? 'mysql',
                    'port' => $_ENV['MYSQL_PORT'] ?? 3306 ,
                ]
            ]);
        }
    ]);
};
