CREATE SCHEMA IF NOT EXISTS nn;

CREATE TABLE nn.users
(
    user_id  INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    status   BOOLEAN      NOT NULL
) ENGINE = INNODB;

CREATE TABLE nn.domain_categories
(
    category_id INT PRIMARY KEY AUTO_INCREMENT,
    name        VARCHAR(255) NOT NULL
) ENGINE = INNODB;

CREATE TABLE nn.courses
(
    course_id INT PRIMARY KEY AUTO_INCREMENT,
    name      VARCHAR(255) NOT NULL,
    timestamp INT          NOT NULL
) ENGINE = INNODB;

CREATE TABLE nn.exercises
(
    exercise_id INT PRIMARY KEY AUTO_INCREMENT,
    course_id   INT          NULL,
    category_id INT          NOT NULL,
    name        VARCHAR(255) NOT NULL,
    points      INT          NOT NULL,

    FOREIGN KEY (course_id)
        REFERENCES nn.courses (course_id)
        ON UPDATE CASCADE ON DElETE RESTRICT,
    FOREIGN KEY (category_id)
        REFERENCES nn.domain_categories (category_id)
        ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE = INNODB;

CREATE TABLE nn.sessions
(
    session_id       INT PRIMARY KEY AUTO_INCREMENT,
    course_id        INT NOT NULL,
    user_id          INT NOT NULL,
    score            INT NOT NULL,
    score_normalized INT NOT NULL,
    start_difficulty INT NOT NULL,
    end_difficulty   INT NOT NULL,
    timestamp        INT NOT NULL,

    FOREIGN KEY (course_id)
        REFERENCES nn.courses (course_id)
        ON UPDATE CASCADE ON DELETE RESTRICT,
    FOREIGN KEY (user_id)
        REFERENCES nn.users (user_id)
        ON UPDATE CASCADE ON DELETE RESTRICT
) ENGINE = INNODB;

CREATE INDEX user_id_timestamp_sessions_idx ON sessions (user_id, timestamp);

INSERT INTO users (user_id, username, password, status)
VALUES (1, 'TEST1', 'secret', 1),
       (2, 'TEST2', 'secret', 1),
       (3, 'TEST3', 'secret', 1),
       (4, 'TEST4', 'secret', 1),
       (5, 'TEST5', 'secret', 1),
       (6, 'TEST6', 'secret', 1),
       (7, 'TEST7', 'secret', 1),
       (8, 'TEST8', 'secret', 1),
       (9, 'TEST9', 'secret', 1)
;

INSERT INTO domain_categories (category_id, name)
VALUES (1, 'memory'),
       (2, 'cognition'),
       (3, 'reflex')
;

INSERT INTO courses (course_id, name, timestamp)
VALUES (1, 'TEST_COURSE1', 1637591448),
       (2, 'TEST_COURSE2', 1637591498)
;

INSERT INTO sessions (session_id, course_id, user_id, score, score_normalized, start_difficulty, end_difficulty,
                      timestamp)
VALUES (1, 1, 1, 10, 1, 1, 10, 1637591448),
       (2, 2, 1, 10, 1, 1, 10, 1637591498)
;

INSERT INTO exercises (exercise_id, course_id, category_id, name, points)
VALUES (1, 1, 1, 'Memory Flex', 10),
       (2, 2, 2, 'Seven Bridges Problem', 15)
;