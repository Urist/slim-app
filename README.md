# slim-app

App based on slim-skeleton

## Setup

1. clone repository
2. `cd slim-app`
3. `docker-compose build`
4. `docker-compose up -d`
5. `docker-compose exec slim composer install`

## Routes

1. http://0.0.0.0:8080/user/1/recentlyTrained - List of most recently trained domain categories.
2. http://0.0.0.0:8080/user/1/history - Scores from up to last 12 sessions.